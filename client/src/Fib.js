import React, { useState, useEffect } from "react";
import axios from "axios";

const Fib = () => {
  const [seenIndexes, setSeenIndexes] = useState([]);
  const [values, setValues] = useState({});
  const [index, setIndex] = useState("");

  useEffect(() => {
    async function fetchValues() {
      const valuesFetched = await axios.get("/api/values/current");
      setValues(valuesFetched);
    }

    async function fetchIndexes() {
      const seenIndexesAll = await axios.get("/api/values/all");
      setSeenIndexes(seenIndexesAll.data);
    }

    fetchValues();
    fetchIndexes();
  }, []);

  async function handleSubmit(e) {
    e.preventDefault();

    axios.post("/api/values", {
      index,
    });
    setIndex("");
  }

  console.log("values", values);
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label>Enter your index</label>
        <input value={index} onChange={(e) => setIndex(e.target.value)} />
        <button>Submit</button>
      </form>
      <h3>Indexes Ive seen: </h3>
      {seenIndexes.map(({ number }) => number.join(", "))}
      <h3>Calculated values: </h3>
      {/* {values?.map((key) => (
        <div key={key}>
          `For index ${key} I calculated ${values[key]}`
        </div>
      ))} */}
    </div>
  );
};

export default Fib;
